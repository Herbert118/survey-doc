[TOC]

http://xx.com

## 总说明

* GET和DELETE方法使用路径传参, POST和PUT使用请求体传参
* 除了登录以外, 全都需要在请求头中加上"Authorization":${token}   (回答问卷应使用独立的token) (token中包含用户身份(权限))
* 时间格式为"YYYY-MM-DD HH:mm:ss"
* code (暂定): 请求成功 801 
* 本文档尚未完善, 仍有很有可能变更
* 本文档和原来的系统已经不兼容!



## 变更

* 0.8 实践3开始
* 0.9 新增验证码, 将xxxName 换成电话号码
* 0.91 改动一下验证码登录;
* 0.92 查询时应返回密保问题; 用户登录和注册时, 应返回租户id;
* 1.0 租户可以对域内问卷进行操作(查询问卷参数增加tenantId); 管理员不可创建租户,答者有可能不可创建,用户似乎不可注册(创建租户 舍弃);  租户可设置问卷可发布数量=>(用户相关响应增加 "问卷数量限制surveyLimit"); 租户可以管理群组(查询群组参数增加tenantId); 修改信息不再有限制(只限制删除, 只保留删除信息, 所有实体添加是否删除deleted); 用户可以添加答者(使用批量导入答者(id)接口); 各个都可修改自己信息(添加总查询queryAll接口);  权限管理仍是问题(放在租户管理中); 还有问卷相关暂时没有修改



## 问题

* **尽管他说按照自己理解来做,但很难保证不会冒出新理解,新需求,一些之后阶段的问题可以搁置**
* 可能会有错误, 如果发现请及时反馈
* 很多细节, 包括命名等 ,仍需商讨
* **系统记录曾经信息, 难道还要查询曾经信息?**(仍存在问题, 先等待; 个人建议是数据库增加一个表(?), 不要在前端体现较好(因为很麻烦))
* **可模糊查询参数有哪些?** =>(可以用多个参数分开查) 
* **验证码具体流程? 注册登录找回是否需要不同的接口?** =>(使用不同参数)
* 答者无答卷? => (理解为发布在所在群组内的问卷)
* 计费管理是否需要单独接口? => (目前放在查询租户里)
* 前端分页?后端分页?=>(前端分页)
* 为了计费方便 , 创建群组和答卷时可能需要tenantId => (确实应使用)
* **这次最好有token,因为有多个身份,需要鉴权** =>(干脆不要, 简化)
* **权限管理, 到底要管租户还是所有身份的用户?**  =>(目前是放在租户管理中, 管理就是修改)
*  
* **自行注册的用户和答者该如何处理仍然是一个问题**(目前选择强制要求选择一个租户/ 群组)
* **电话号码, 群组名等是否应保证不重复?**=>(能不重复最好)
* &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
* 推荐问卷? 由谁推荐?=> (显示最新的)
* 是否还需要让问卷关联群组?(目前先用以前的, 关联一下)
* 问卷应当可以限制群  组内的人回答问卷; 那又是否可以发布不限群组的广泛问卷?(目前没有定论,也没有设计)
* 生成链接? 根据问卷链接答卷?那如果问卷发布在群组内,答者能否登录后看到自身需要回答的问卷列表, 不去点击链接而选择主动答卷?(目前认为可以)
* 考虑到可用性, 是否可以考虑生成二维码?



* 

## 登录权限管理

### 密码登录

* URL: `/pwLogin`
* method: POST
* 参数:
   * 账号 account
   * 密码 password
* 响应:
   * 代码 code
   * 信息 message
   * userData
     * 用户ID id
     * 用户身份 role
     * 用户token token
     

### 发送验证码

* URL:`/sendSms`
* method:GET
* 参数:
  * 手机号 phoneNumber 
  * 目的 purpose (["login","register","retrieval"])
* 响应:
  * 代码 code
  * 信息 message
* 说明: 登录和找回时, 若手机号未注册, 则不发送,并进行提示; 注册则相反; 另外,可能需要人机验证?

### 验证码登录

* URL:`/smsLogin`
* method:POST
* 参数:
  * 手机号 phoneNumber
  * 验证码 veriCode 

* 响应:
  * 代码 code
  * 信息 message
  * userData
    * 用户ID id
    * 用户身份 role
    * 用户token token
    * (若为用户)所属租户id tenantId

### 	注册

* URL: `/register`

* method: POST

* 参数:
  * 账号 account
  
  * 密码 password
  
  * 手机号 phoneNumber
  
  * 验证码 veriCode
  
  * 身份 role(["admin","tenant","user","replier"])
  
  * (可选)密保问题 securityQuestion
  
  * (可选)密保答案 securityAnswer
  
  * (若为用户)租户账号 tenantAccount
  
  * (若为答者) 群组名 groupName
  
    
  
* 响应:
  * 代码 code
  * 信息 message
  
* 说明: 使用哪种密保仍待讨论; 可能手机号还需验证;    账户名,电话号码应保证不重复; 

### 	密保密码找回 

* URL: `/secRetrieval`
* method: POST
* 参数:
  * 账号 account
  * 密保答案 securityAnswer
  * 响应:
  
  * 代码 code
  * 信息 message

### 验证码密码找回

* URL: `/smsRetrieval`
* method: POST
* 参数:
  * 手机号 phoneNumber
  * 验证码 veriCode
* 响应:
  * 代码 code
  * 信息 message

### 	权限管理

(目前放在租户管理中)



### 总查询

* URL: `/queryAll`
* method: GET
* 参数:
  * (可选)电话号码 phoneNumber 
  * (可选)账户 account
  * (可选)id
* 响应:
  * 代码 code
  * 信息 message
  * 任意用户信息 info
    * role 角色
    * id
    * 账户 account
    * 电话号码 phoneNumber
    * 状态 status(["true","false"])(是否可修改)
    * 密保问题 securityQuestion
    * 密保答案 securityAnswer
    * (若租户)费用 bill  
    * (若租户)权限 authority(["true","false"])(?)
    * (若用户)问卷数量限制 surveyLimit
    * (若用户)租户id tenantId
    * (若答者)groupIdList
    * 
* 说明: 待完善



## 租户管理
### 创建租户(舍弃)
* URL: `/addTenant`
* method: POST
* 参数:
  * 租户电话号码 phoneNumber
  * 账号 account
  * 密码 password
  * 
  
* 响应:
  * 代码 code
  * 信息 message
* ;说明: 保留接口以便调试

### 查询租户

* URL: `/queryTenantList`
* method: GET
* 参数:
  * (可选,可模糊)租户电话号码 phoneNumber 
  * (可选,可模糊)账户 account
  * 其他参数(?)
* 响应:
  * 代码 code
  * 信息 message
  * 租户列表 tenantList
    * 租户id id
    * 账户 account
    * 租户电话号码 phoneNumber
    * 费用 bill  
    * 权限 authority(["true","false"])(?)
    * 状态 status(["true","false"])(是否可修改)
    * 密保问题 securityQuestion
    * 密保答案 securityAnswer
    * 是否删除 deleted
* 说明: 

### 修改租户

* URL: `/modifyTenant`
* method: PUT
* 参数:
  * 租户id id
  * (可选)租户电话号码 phoneNumber
  * (可选)密码 password
  * (可选)权限 authority(["true","false"])(?)
  * (可选)密保问题 securityQuestion
  * (可选)密保答案 securityAnswer
  * (可选) 是否删除 deleted
* 响应:
  * 代码 code
  * 信息 message
* 说明:

### 删除租户

* URL: `/deleteTenant`
* method: DELETE
* 参数:
  * 租户id id
* 响应:
  * 代码 code
  * 信息 message
* 说明:租户在无数据关联时删除。 系统记录曾经信息

### 查询删除修改信息(?)

(先不管)

## 用户管理
### 创建用户
* URL: `/addUser`
* method: POST
* 参数:
  * 帐号 account
  * 密码 password
  * 手机号 phoneNumber
  * 问卷数量限制 surveyLimit
  * 所属租户id tenantId
* 响应:
  * 代码 code
  * 信息 message
* 说明: 可批量导入 可能可以补充其他信息; 相对于注册,验证码与密保问题和答案可置空  账户名,电话号码应保证不重复

### 批量导入用户

* URL: `/addMutiUserByEx`

* method: POST

* 参数:
  
  * 所属租户id
  
  * (excel?)
    * 帐号 account
  
    * 密码 password
  
    * 用户电话号码 phoneNumber
  
    * 问卷数量限制 surveyLimit
    
      
  
* 响应:
  * 代码 code
  * 信息 message

* 说明: 可批量导入 可能可以补充其他信息; 相对于注册,验证码与密保问题和答案可置空;   电话号码应保证不重复

### 查询用户

* URL: `/queryUserList`
* method: GET
* 参数:
  * (可选)所属租户id tenantId
  * (可选,可模糊)用户电话号码 phoneNumber
  * 其他(?)
* 响应:
  * 代码 code
  * 信息 message
  * 用户列表 userList
    * 用户id id
    * 所属租户id tenantId
    * 用户账户 account
    * 用户电话号码 phoneNumber
    * 状态 status(["true","false"])(是否可修改)
    * 问卷数量限制 surveyLimit
    * 密保问题 securityQuestion
    * 密保答案 securityAnswer
    * 是否删除 deleted
* 说明: 用户在无群组无问卷状态下支持修改,删除; 

### 修改用户

* URL: `/modifyUser`
* method: PUT
* 参数:
  * 用户id id
  * (可选)密码 password
  * (可选)用户电话号码 phoneNumber
  * (可选)租户id tenantId(作冗余,以防变更)
  * (可选)密保问题 securityQuestion
  * (可选)密保答案 securityAnswer
  * (可选)问卷数量限制 surveyLimit
  * (可选)是否删除 deleted
  
* 响应:
  * 代码 code
  * 信息 message
* 说明: 

### 删除用户

* URL: `/deleteUser`
* method: DELETE
* 参数:
  * 用户id id
* 响应:
  * 代码 code
  * 信息 message
* 说明:用户在 删除。 系统记录曾经信息

### 查询删除修改信息(?)

(先不管)

## 答者管理
### 创建答者
* URL: `/addReplier`

* method: POST

* 参数:
  * 账号 account
  * 密码 password
  * 答者电话号码 phoneNumber
  * (可选)所属群组id groupId
  
* 响应:
  * 代码 code
  
  * 信息 message
  
    
  
* 说明: 可批量导入 可能可以补充其他信息; 相对于注册,验证码与密保问题和答案可置空; 电话号码是否应保证不重复?

### 批量导入答者(从excel文件)(存疑)

* URL: `/addMutiReplierByEx`
* method: POST
* 参数:
  * 导入的群组id groupId
  * (excel列表?)
    * 账号 account
    * 密码 password
    * 答者电话号码 phoneNumber
    * 
* 响应:
  * 代码 code
  * 信息 message
* 说明: 如果账号已存在, 就从现存答者导入(忽略电话号和密码, 或者修改,自行决定),若不存在, 就创建 ;  电话号码应保证不重复

* 

### 批量导入答者(从现存答者id)

* URL: `/addMutiReplierById`
* method: POST
* 参数:
  * 导入的群组id groupId
  * 答者id列表 replierIdList
    * 
* 响应:
  * 代码 code
  * 信息 message
* 说明: 可能可以补充其他信息; 相对于注册,验证码与密保问题和答案可置空;

### 查询答者

* URL: `/queryReplierList`
* method: GET
* 参数:
  * (可选)所属群组id列表 groupIdList
  * (可选,可模糊)用户电话号码 phoneNumber
  * (可选) 问卷id (查询回答该问卷的问卷)
  * 其他(?)
* 响应:
  * 代码 code
  * 信息 message
  * 答者列表 replierList
    * 答者id id
    * 所属群组id列表 groupIdList
    * 答者账户 account
    * 答者电话号码 phoneNumber
    * 密保问题 securityQuestion
    * 密保答案 securityAnswer
    * 是否删除 deleted
* 说明: 用户在无群组无问卷状态下支持修改,删除; 

### 修改答者

* URL: `/modifyReplier`
* method: PUT
* 参数:
  * 答者id
  * (可选)密码 password
  * (可选)答者电话号码 phoneNumber
  * 答者所属群组列表 groupIdList(作冗余,以防变更 应该不会直接用,可不实现)
  * (可选)密保问题 securityQuestion
  * (可选)密保答案 securityAnswer
  * (可选)是否删除 deleted
* 响应:
  * 代码 code
  * 信息 message
* 说明:  答者在无群组无答卷状态下支持修改 、 删除。 系统记录曾经信息

### 删除答者(舍弃)

* URL: `/deleteReplier`
* method: DELETE
* 参数:
  * 答者id id
* 响应:
  * 代码 code
  * 信息 message
* 说明:似乎不可删除, 但保留接口以便调试

### 查询删除修改信息(?)

(先不管)

## 群组管理

### 创建群组

* URL: `/addGroup`
* method: POST
* 参数:
  * 群组名 groupName
  * 所属用户id userId
  * 所属租户id tenantId  (应当要有,多对多,不好追溯)
  * 群组说明: groupDescription
* 响应:
  * 代码 code
  * 信息 message
* 说明: 群组名应保证不重复; 注意追溯和计费

### 查询群组

* URL: `/queryGroupList`

* method: GET

* 参数:
  
  * (可选)所属用户id userId
  * (可选)所属租户id tenantId
  * (可选, 可模糊)groupName
  * 其他(?)
  
* 响应:
  * 代码 code
  * 信息 message
  * groupList
    * 群组ID id
    * 所属用户id userId
    * 群组名 groupName
    * 群组说明: groupDescription
    * 所属租户id tenantId
    * 群组状态: status (是否有答者,(是否有答卷?)) 
    * 是否删除 deleted
  * 说明:
  
  

### 修改群组

* URL: `/modifyGroup`
* method: PUT
* 参数:
  * 群组ID id
  * (可选)群组名 groupName
  * (可选)群组描述 groupDescription
  * (可选)是否删除 deleted
* 响应:
  * 代码 code
  * 信息 message
* 说明: 



### 删除群组

* URL: `/deleteGroup`
* method: DELETE
* 参数:
  * 群组ID id
* 响应:
  * 代码 code 
  * 信息 message
* 说明:群组在无数据关联时删除。 系统记录曾经信息

### 查询删除修改信息(?)

(先不管)

## 问卷管理&数据分析(暂用之前实现)

### 创建问卷(模板)

* URL: `/addSurvey`
* method: POST
* 参数:
  * 问卷名 surveyName
  * 问卷内容 surveyContent (json)
  * 所属用户id userId
  * 所属租户id tenantId(计费)
* 响应:
  * 代码 code
  * 信息 message
* 说明:(暂时使用从前的实现)创建未关联问卷,或者说模板; 注意追溯和计费

### 关联问卷

* URL: `/linkSurvey`
* method: POST
* 参数:
  * 问卷id surveyId
  * 开始时间 startTime
  * 结束时间 endTime
  * 群组ID groupId
* 响应:
  * 代码 code
  * 信息 message
* 说明:(暂时使用从前的实现)关联群组和问卷, 会复制模板问卷再关联; 注意追溯和计费

### 查询问卷列表

* URL: `/querySurveyList`
* method: GET
* 参数:
  * (可选)群组ID groupId   (不加则查询模板)
  * (可选)答者ID replierId (加了则显示答者该回答的问卷)
  
* 响应:
  * 代码 code
  * 信息 message
  * 问卷列表 surveyList
    * 代码 code
    * 信息 message
    * 问卷列表 surveyList
      * 问卷id id
      * 问卷名 surveyName
      * (可选)问卷状态 surveyStatus
      * (可选)开始时间 startTime
      * (可选)结束时间 endTime
      * 租户id tenantId
* 说明: (暂时使用从前的实现)查询问卷列表, 群组内或者模板或者是答者需要的; 

### 查询单个问卷

* URL: `/querySurveyById`
* method: GET
* 参数:
  * 问卷ID surveyId
  * (可选)群组id groupId
* 响应:
  * 代码 code 
  * 信息 message
  * 问卷信息 surveyData
    *  问卷id surveyId
    *  问卷名 surveyName
    *  问卷内容 surveyContent (json)
    *  (可选)问卷状态 surveyStatus
    *  (可选)开始时间 startTime
    *  (可选)结束时间 endTime
    *  租户id tenantId
* 说明:回答或者预览问卷时, 获得问卷内容

### 修改问卷

* URL: `/modifySurvey`
* method: PUT
* 参数:
  * 问卷ID surveyId
  * (可选)问卷名 surveyName
  * (可选)问卷内容 surveyContent(json)
  * 
  * (可选)群组ID groupId  (存在则修改群组内问卷)
  * (可选)开始时间 startTime
  * (可选)结束时间 endTime
  * (可选)问卷状态 surveyStatus
* 响应:
  * 代码 code 
  * 信息 message
* 说明:若问卷正在开启中, 则不能修改



###  删除问卷

* URL: `/deleteSurvey`
* method: DELETE
* 参数:
  * 问卷ID surveyId
* 响应:
  * 代码 code 
  * 信息 message
* 说明:若问卷正在开启中, 则不能删除

### 回答问卷

* URL: `/answerSurvey`
* method: POST
* 参数:
  * 问卷ID surveyId
  * 问卷答案 answer (json)
  * 回答开始时间 answerStartTime
  * 回答结束时间 answerEndTime
  * ip地址 ipAddress
* 响应:
  * 代码 code 
  * 信息 message
* 说明:答者点击链接后，还需要先登录获取token才能答题; 或者若问卷限制为群组内,可以直接登录从列表中回答问卷



### 发布问卷

* URL: `/sendSurvey`
* method: POST
* 参数:
  * 问卷id surveyId
  * 发送时间 sendTime
  * 发送内容 context
* 响应:
  * 代码 code
  * 信息 message
  * 链接 link
* 说明: 答者点击链接后，还需要先登录获取token才能答题;  或者若问卷限制为群组内,可以直接登录从列表中回答问卷



### 查询问卷结果

* URL: `/querySurveyResult`
* method: GET
* 参数:
  * 问卷ID id
* 响应:
  * 代码 code
  * 信息 message
  * surveyResult(json)
* 说明:根据问卷id查询问卷结果





































